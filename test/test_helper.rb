ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

 def is_logged_in?
    !session[:user_id].nil?  
 end
 
 #Logs in a test user
 def log_in_as(user, options = {})
   pass = options[:password] || 'password'
   re_me = options[:remember_me] || '1'
   if integration_test?
     post login_path, session: {name: user.name, password: pass, remember_me: re_me}
   else
     session[:user_id] = user.id
   end
 end
 
 private
  
  #returns true inside an integration test
  def integration_test?
    defined?(post_via_redirect)
  end
end
