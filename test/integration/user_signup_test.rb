require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    ActionMailer::Base.deliveries.clear
  end
  
  test "invalid submission" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: {name: "", email: "example@com.pl", 
      email_confirmation: "examp@com.pl", password: "example", password_confirmation: "example"} 
    end
    assert_template 'users/new'
  end
  
  test "valid submission" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, user: {name: "example", email: "example@com.pl", 
      email_confirmation: "example@com.pl", password: "example", password_confirmation: "example"}
    end
    
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    # Try to log in before activation.
    log_in_as(user)
    assert_not is_logged_in?
    # Invalid activation token
    get edit_account_activation_path("invalid token")
    assert_not is_logged_in?
    # Valid token, wrong email
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # Valid activation token
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'public/index'
    assert is_logged_in?
  end
end
