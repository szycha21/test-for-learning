require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:example)
  end
  
  test "unsuccesfull edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: {name: "", email: "invalid@mail"}
    assert_template 'users/edit'
  end
  
  test "succesfull edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name = "Correct name"
    email = "correct@pass.com"
    patch user_path(@user), user: {name: name, email: email, email_confirmation: email, password: "", password_confirmation: ""}
    assert_not flash.empty?
    assert_redirected_to root_path
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
end
