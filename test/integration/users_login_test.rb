require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:example)
  end
  
  test "login with invalid informaiton" do
    get login_path
    assert_template 'session/new'
    post login_path, session: {name: " ", password: "example@test,com"}
    assert_template 'session/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test "login with valid information followerd by logout" do
    get login_path
    assert_template 'session/new'
    post login_path, session: { name: @user.name, password: 'password'}
    assert is_logged_in?
    assert_redirected_to users_path
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    #simulate user clicking logout in second window
    delete logout_path
    assert_redirected_to root_url
  end
  
  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_nil cookies['remember_token']
  end
  
  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end
end
