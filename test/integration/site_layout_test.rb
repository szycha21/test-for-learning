require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" od
  #   assert true
  # end
  test "check links" do
    get root_path
    assert_template 'public/index'
    assert_select "a[href=?]", info_path
    assert_select "a[href=?]", comparsion_path
    assert_select "a[href=?]", articles_path
    assert_select "a[href=?]", signup_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path
    get info_path
    assert_template 'public/info'
    assert_select "a[href=?]", root_path
    get comparsion_path
    assert_select "a[href=?]", root_path
    get articles_path
    assert_select "a[href=?]", root_path
    get login_path
    assert_select "a[href=?]", root_path
  end
end
