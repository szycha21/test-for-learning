require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "example", email: "example@test.com", password: "example")
  end
  
  test "should_be_valid" do
    assert @user.valid?
  end
  
  test "name could not be blank" do
    @user.name = " "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = "  "
    assert_not @user.valid?
  end
  
  test "name can not bo too long" do
    @user.name = 'a' * 50
    assert_not @user.valid?
  end
  
  test "email can not be too long" do
    @user.email = 'a' * 50 + '@example.com'
    assert_not @user.valid?
  end
  
  test "accept valid adresses" do
    addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
      addresses.each do |address|
        @user.email = address
        assert @user.valid?, "#{address.inspect} should be valid"
      end
  end
  
  test "do not accept invalid addresses" do
    addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |address|
        @user.email = address
        assert_not @user.valid?, "#{address.inspect} should not be valid"
      end
  end
  
  test "uniqueness validation" do
    new = @user.dup
    new.email = @user.email.upcase
    @user.save
    assert_not new.valid?
    
  end
  
  test "pass can not be blank" do
    @user.password = " "
    assert_not @user.valid?
  end
  
  test "password to short" do
    @user.password = 'a' * 5
    assert_not @user.valid?
  end
  
  test "email should be downcase" do
    testmail = "ExaMple@TEST.com"
    @user.email = testmail
    @user.save
    assert_equal testmail.downcase, @user.email
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  
  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Example text")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  
  test "should follow and unfollow a user" do
    user = users(:example)
    other  = users(:third)
    assert_not user.following?(other)
    user.follow(other)
    assert user.following?(other)
    assert other.followers.include?(user)
    user.unfollow(other)
    assert_not user.following?(other)
  end
  
  test "feed should have the right posts" do
    user = users(:example)
    second = users(:secondex)
    third = users(:third)
    #posts from followed
    second.microposts.each do |post_following|
      assert user.feed.include?(post_following)
    end
    
    #post from self
    user.microposts.each do |post_self|
      assert user.feed.include?(post_self)
    end
    
    #post from unfollowed
    third.microposts.each do |post_unfollowed|
      assert_not user.feed.include?(post_unfollowed)
    end
  end
end
