class SessionController < ApplicationController
  
  layout false
  def new
  end
  
  def create
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_to users_path
      else
        message = "Account not activated"
        message += "Check your email"
        flash[:warning] = message
        redirect_to root_url
      end
    else
      #create error message
      flash.now[:danger] = 'Invalid name/password'
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
