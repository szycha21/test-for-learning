class PublicController < ApplicationController
    
    layout 'application'
    def index
        @micropost = current_user.microposts.build if logged_in?
    end
end
